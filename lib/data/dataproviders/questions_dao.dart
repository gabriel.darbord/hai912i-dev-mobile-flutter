import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:tp3_firebase/data/models/question_model.dart';

class QuestionsDAO {
  late final CollectionReference<Map<String, dynamic>> db;

  Future<List<Question>> init() async {
    await Firebase.initializeApp();
    db = FirebaseFirestore.instance.collection('questions');
    return getQuestionsTheme("histoire");
  }

  Future<List<Question>> getQuestionsTheme(String theme) async {
    Future<List<Question>> questions = Future(() => <Question>[]);
    await db.doc(theme).get().then((doc) {
      List<dynamic> values = doc.data()!['questions'];
      for (var json in values) {
        questions.then((value) => value.add(Question.fromJSON(json)));
      }
    });
    return questions;
  }
}

class Question {
  late String texte;
  late bool estCorrecte;

  Question({required this.texte, required this.estCorrecte});

  Question.fromJSON(Map<String, dynamic> json) {
    texte = json['question'];
    estCorrecte = json['reponse'];
  }

  Map<dynamic, dynamic> toJson() => <dynamic, dynamic>{
        'question': texte,
        'reponse': estCorrecte,
      };
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'business_logic/cubits/theme_cubit.dart';
import 'presentation/pages/home_page.dart';
import 'business_logic/cubits/indice_question_cubit.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ThemeCubit>(
      create: (context) => ThemeCubit(ThemeData.dark()),
      child: BlocBuilder<ThemeCubit, ThemeData>(
        builder: (context, theme) => MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'TP4',
          theme: theme,
          home: BlocProvider<IndiceQuestionCubit>(
            create: (context) => IndiceQuestionCubit(),
            child: HomePage(title: 'Quiz'),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter_bloc/flutter_bloc.dart';

class IndiceQuestionCubit extends Cubit<int> {
  IndiceQuestionCubit() : super(0);

  next(int indiceMax) {
    if (state + 1 == indiceMax) {
      emit(0);
    } else {
      emit(state + 1);
    }
  }
}

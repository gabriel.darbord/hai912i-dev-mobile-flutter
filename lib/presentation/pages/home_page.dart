import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tp3_firebase/data/dataproviders/questions_dao.dart';
import 'package:tp3_firebase/data/models/question_model.dart';
import 'package:tp3_firebase/presentation/widgets/question_widget.dart';
import 'package:tp3_firebase/presentation/widgets/theme_switch.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key, required this.title}) : super(key: key) {
    QuestionsDAO dao = QuestionsDAO();
    questionsFuture = dao.init();
  }

  final String title;
  late final Future<List<Question>> questionsFuture;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: const [
          ThemeSwitch(),
        ],
        title: Text(title),
      ),
      body: Container(
        alignment: Alignment.center,
        margin: const EdgeInsets.only(top: 10, bottom: 100),
        child: Column(
          children: [
            const Image(
              width: 220,
              height: 220,
              image: NetworkImage(
                  "https://us.123rf.com/450wm/yupiramos/yupiramos1909/yupiramos190990710/130493238-eiffel-tower-with-flag-of-france-happy-bastille-day-flat-design-vector-illustration.jpg"),
            ),
            FutureBuilder(
                future: questionsFuture,
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return const Text('Erreur de chargement des questions');
                  } else if (!snapshot.hasData ||
                      snapshot.connectionState != ConnectionState.done) {
                    return const CircularProgressIndicator();
                  }
                  var questions = snapshot.data as List<Question>;
                  return QuestionWidget(questions: questions);
                }),
          ],
        ),
      ),
    );
  }
}

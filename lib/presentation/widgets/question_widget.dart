import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tp3_firebase/business_logic/cubits/indice_question_cubit.dart';
import 'package:tp3_firebase/data/models/question_model.dart';

class QuestionWidget extends StatelessWidget {
  const QuestionWidget({Key? key, required this.questions}) : super(key: key);

  final List<Question> questions;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: BlocBuilder<IndiceQuestionCubit, int>(builder: (context, indice) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              width: 300,
              padding: const EdgeInsets.only(
                  top: 20, bottom: 20, left: 10, right: 10),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(20),
              ),
              child: Text(
                questions[indice].texte,
                style: const TextStyle(
                  //color: Colors.white,
                  fontStyle: FontStyle.italic,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  child: const Text("Vrai"),
                  onPressed: () => _checkAnswer(context, true, indice),
                ),
                ElevatedButton(
                  child: const Text("Faux"),
                  onPressed: () => _checkAnswer(context, false, indice),
                ),
                ElevatedButton(
                  child: const Icon(Icons.arrow_forward),
                  onPressed: () => BlocProvider.of<IndiceQuestionCubit>(context)
                      .next(questions.length),
                ),
              ],
            ),
          ],
        );
      }),
    );
  }

  _checkAnswer(BuildContext context, bool userChoice, int indiceQuestion) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        duration: const Duration(seconds: 1),
        content: Text(userChoice == questions[indiceQuestion].estCorrecte
            ? "Oui"
            : "Non"),
      ),
    );
  }
}

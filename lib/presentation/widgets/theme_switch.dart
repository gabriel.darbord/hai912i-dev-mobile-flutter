import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tp3_firebase/business_logic/cubits/theme_cubit.dart';

class ThemeSwitch extends StatefulWidget {
  const ThemeSwitch({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => ThemeSwitchState();
}

class ThemeSwitchState extends State<StatefulWidget> {
  bool _value = true;

  @override
  Widget build(BuildContext context) {
    return Switch(
      value: _value,
      onChanged: (bool value) {
        setState(() {
          _value = value;
        });
        ThemeData theme;
        if (value) {
          theme = ThemeData.dark();
        } else {
          theme = ThemeData.light();
        }
        BlocProvider.of<ThemeCubit>(context).change(theme);
      },
    );
  }
}
